<div class="datagrid-wrapper card shadow-sm" id="{{ $tableId }}_wrapper">
    <div class="card-body">
        <div class="datagrid-toolbar">
            <div class="search-filter-wrapper">
                <div class="search-component">
                    <input type="search" wire:model="searchText" wire:keydown.enter="search" class="form-control" placeholder="{{ __('livewire-datatables::datatables.labels.search-input') }}">

                    @if($searchText)
                        <span wire:click="resetSearch" class="reset-btn">
                            <i class="{{ config('livewire-datatables.ui.icons.search-cancel') }}"></i>
                        </span>
                    @endif
                </div>

                @if(!empty($filters))
                    <button class="{{ config('livewire-datatables.ui.classes.btn-filter') }}" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDatagridFilter">
                        <i class="{{ config('livewire-datatables.ui.icons.filter-btn') }}"></i>
                        <span class="d-none d-sm-inline">
                            {{ __('livewire-datatables::datatables.labels.filters-btn') }}
                            @if(!empty($this->hasActiveFilters())) <span class="ms-1 badge bg-danger">{{ $this->activeFiltersCount() }}</span> @endif
                        </span>
                    </button>
                @endif
            </div>

            <ul class="pagination m-0">
                <li class="page-item {{ $page > 1 ? '' : 'disabled' }}">
                    <a class="page-link px-3 px-sm-4" wire:click="setPage({{ $page - 1 }})" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>

                <li class="display d-none d-sm-block px-0"
                    title="{{ __('livewire-datatables::datatables.labels.items') }}: {{ ($page-1)*$pageLimit }} - {{ $page*$pageLimit  }} ({{ __('livewire-datatables::datatables.labels.total') }}: {{ $rowsTotal }})">
                        <span class="page-link px-sm-3">{{ $page }} / {{ $pagesTotal }}</span>
                </li>

                <li class="page-item {{ $page >= $pagesTotal ? 'disabled' : '' }}">
                    <a class="page-link px-3 px-sm-4" wire:click="setPage({{ $page + 1 }})" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="datagrid-table-wrapper">
            <table class="table table-centered table-nowrap rounded">
                <thead class="thead-light">
                    <tr>
                        @if(!empty($batchActions))
                            <th class="batch-all-toggler">
                                <a wire:click="toggleAllRowsSelection" class="px-2 py-1">
                                    <i class="{{ $this->hasAllSelectedRows() ? config('livewire-datatables.ui.icons.checked-square') : config('livewire-datatables.ui.icons.unchecked-square') }}"></i>
                                </a>
                            </th>
                        @endif
                        @foreach($columnsConfig as $col)
                            @if($this->isColumnVisible($col))
                                @if($col->isSortable())
                                    <th class="sortable {{ ($col->getSelectName() === $this->sortColumn ? 'sorting-active' : '') }} {{ $col->thClass }}">
                                        <a wire:click="sortBy('{{ $col->getSelectName() }}')">
                                            {{ $col->getLabel() }}
                                            @if($col->getSelectName() === $this->sortColumn)
                                                <i class="ms-1 {{ $this->sortDirection === 'desc' ? config('livewire-datatables.ui.icons.sort-desc') : config('livewire-datatables.ui.icons.sort-asc') }}"></i>
                                            @else
                                                <i class="ms-1 text-muted {{ config('livewire-datatables.ui.icons.sort-inactive') }}"></i>
                                            @endif
                                        </a>
                                    </th>
                                @else
                                    <th class="{{ $col->thClass }}">{{ $col->getLabel() }}</th>
                                @endif
                            @endif
                        @endforeach

                        <th class="settings-col {{ !empty($actionButtons) ? 'with-action-buttons' : '' }}">
                            <a data-bs-toggle="offcanvas" data-bs-target="#offcanvasDatagridSettings" class="settings-btn">
                                <i class="{{ config('livewire-datatables.ui.icons.table-settings') }}"></i>
                            </a>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @if(!empty($rows))
                        @foreach($rows as $row)
                            <tr wire:key="row_{{ $row["id"] }}" class="{{ $this->isItemSelected($row["id"]) ? 'batch-selected-row' : ''}}">
                                @if(!empty($batchActions))
                                    <td class="batch-item-toggler">
                                        <a wire:click="toggleRowSelection('{{ $row["id"] }}')" class="p-2">
                                            <i class="shadow-sm {{ $this->isItemSelected($row["id"]) ? config('livewire-datatables.ui.icons.checked-square') . ' text-info' : config('livewire-datatables.ui.icons.unchecked-square') . ' text-muted' }}"></i>
                                        </a>
                                    </td>
                                @endif

                                @foreach($columnsConfig as $col)
                                    @if($this->isColumnVisible($col))
                                        <td class="{{ $col->tdClass }}">
                                            @if($col->isEnabledUnescaped())
                                                {!! $col->formatValue($row[$col->getSelectName() ?? null]) !!}
                                            @else
                                                {{--@dd($row)--}}
                                                {{ $col->formatValue($row[$col->getSelectName()] ?? null) }}
                                            @endif
                                        </td>
                                    @endif
                                @endforeach

                                @if(!empty($actionButtons))
                                    <td class="action-buttons">
                                        @foreach($actionButtons as $btn)
                                            {!! $btn->setRowItem($row)->htmlOutput($row) !!}
                                        @endforeach
                                    </td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr class="no-records-pane">
                            <td colspan="{{ count($columnsConfig) + (int)(!empty($batchActions)) + (int)(!empty($actionButtons))  }}" class="text-center text-muted py-4">
                                @if(!empty($searchText))
                                    <p>
                                        <i class="{{ config('livewire-datatables.ui.icons.search-empty-label') }}"></i>
                                        <br>
                                        {{ __('livewire-datatables::datatables.messages.search-empty-results') }}
                                    </p>
                                    <button type="button" class="btn btn-sm btn-outline-warning my-3" wire:click="resetSearch">
                                        <i class="{{ config('livewire-datatables.ui.icons.reset-search-btn') }} me-1"></i>
                                        {{ __('livewire-datatables::datatables.labels.reset-search') }}
                                    </button>
                                @elseif($this->hasActiveFilters())
                                    <p>
                                        <i class="{{ config('livewire-datatables.ui.icons.filter-empty-results') }}"></i>
                                        <br>
                                        {{ __('livewire-datatables::datatables.messages.filter-empty-results') }}
                                    </p>
                                    <button type="button" class="btn btn-sm btn-outline-warning my-3" wire:click="filterClear">
                                        <i class="{{ config('livewire-datatables.ui.icons.reset-filters-btn') }} me-1"></i>
                                        {{ __('livewire-datatables::datatables.labels.clear-all-filters') }}
                                    </button>
                                @elseif($rowsTotal !== 0)
                                    <p>
                                        <i class="{{ config('livewire-datatables.ui.icons.no-records-label') }}"></i>
                                        <br>
                                        {{ __('livewire-datatables::datatables.messages.no-records') }}
                                    </p>
                                @else
                                    <p>
                                        <i class="{{ config('livewire-datatables.ui.icons.loading-data') }}"></i>
                                        <br>
                                        {{ __('livewire-datatables::datatables.messages.loading-data') }}
                                    </p>
                                @endif
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasDatagridSettings" data-bs-scroll="true" style="width: 280px">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title">{{ __('livewire-datatables::datatables.labels.table-settings') }}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="{{ __('livewire-datatables::datatables.labels.close') }}"></button>
        </div>
        <div class="offcanvas-body">
            <div class="form-floating mb-3">
                <select wire:model="rowsPerPage" id="dg_setting_page_limit" class="form-select">
                    @foreach(config('livewire-datatables.settings.pagination-options') as $num)
                        @if($num === 0)
                            <option value="0">{{ __('livewire-datatables::datatables.labels.rows-auto-size') }}</option>
                        @else
                            <option value="{{ $num }}">{{ $num }} {{ __('livewire-datatables::datatables.labels.rows') }}</option>
                        @endif
                    @endforeach
                </select>
                <label for="dg_setting_page_limit">{{ __('livewire-datatables::datatables.labels.rows-per-page') }}</label>
            </div>

            <div class="form-floating checkbox-group mb-3">
                <label>{{ __('livewire-datatables::datatables.labels.visible-columns') }}</label>
                <div class="options-wrapper">
                    @foreach($columnsConfig as $col)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" wire:model="columnsVisibility.{{ $col->getSelectName() }}" id="columnsVisible_{{ $col->getSelectName() }}">
                            <label class="form-check-label" for="columnsVisible_{{ $col->getSelectName() }}">{{ $col->getLabel() }}</label>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="text-center">
                <button type="button"
                    class="{{ config('livewire-datatables.ui.classes.btn-settings-save') }}"
                    wire:click="saveTableSettings"
                    @click="(new bootstrap.Offcanvas('#offcanvasDatagridSettings')).hide()">
                        <i class="{{ config('livewire-datatables.ui.icons.settings-save-btn') }} me-1"></i> {{ __('livewire-datatables::datatables.labels.save-settings-btn') }}
                </button>
            </div>
        </div>
    </div>

    @if(!empty($filters))
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasDatagridFilter" data-bs-scroll="true" style="width: 280px">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title">{{ __('livewire-datatables::datatables.labels.filter') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <button type="button"
                    class="{{ config('livewire-datatables.ui.classes.btn-clear-filters') }}"
                    wire:click="filterClear"
                    @click="(new bootstrap.Offcanvas('#offcanvasDatagridFilter')).hide()">
                        <i class="{{ config('livewire-datatables.ui.icons.clear-all-filters-btn') }} me-1"></i>
                        {{ __('livewire-datatables::datatables.labels.clear-all-filters') }}
                </button>
                <hr>
                @foreach($filters as $filter)
                    <div class="form-floating mb-3">
                        {!! $filter->htmlOutput() !!}
                        <label for="dg_filter_{{ $filter->getName() }}">{{ $filter->getLabel()  }}</label>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    @if(!empty($batchActions))
        <div class="position-fixed shadow card {{ $this->hasSelectedRows() ? 'd-block' : 'd-none' }}" id="datagridPanelBatchActions" style="width: 250px; bottom: 1rem; right: 1rem;">
            <div class="card-header ps-3 pe-2 py-2 d-flex justify-content-between align-items-center">
                <h5 class="card-title mb-0 fs-7">
                    {{ trans_choice('livewire-datatables::datatables.labels.selected-n-items', $this->selectedRowsCount(), ['num' => $this->selectedRowsCount()]) }}
                </h5>
                <button type="button"
                    class="btn-close"
                    wire:click="cancelBatchActionsSelection"
                    aria-label="{{ __('livewire-datatables::datatables.labels.close') }}"></button>
            </div>
            <div class="card-body pb-1 text-center">
                @foreach($batchActions as $action)
                    <button type="button" class="btn btn-{{ $action->variant }} mb-2 w-100" wire:click="doBatchAction('{{ $action->name }}')">
                        <i class="bi {{ $action->iconClass }} me-1"></i> {{  $action->label }}
                    </button>
                @endforeach
            </div>
        </div>
    @endif
</div>
