<div class="datagrid-wrapper card shadow-sm  placeholder-wave" id="{{ $tableId }}_wrapper">
    <div class="card-body">
        <div class="datagrid-toolbar">
            <div class="search-filter-wrapper">
                <div class="search-component">
                    <input type="search" class="form-control placeholder">
                </div>

                <button class="btn ms-2 placeholder bg-secondary-subtle" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDatagridFilter">
                    <i class="{{ config('livewire-datatables.ui.icons.filter-btn') }}"></i> <span class="d-none d-sm-inline">{{ __('livewire-datatables::datatables.labels.filters-btn') }}</span>
                </button>
            </div>

            <ul class="pagination m-0">
                <li class="page-item placeholder bg-white">
                  <a class="page-link px-3 px-sm-4" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>

                <li class="display d-none d-sm-block px-0 placeholder"><span class="page-link px-sm-3">1 / 1</span></li>

                <li class="page-item placeholder bg-white">
                  <a class="page-link px-3 px-sm-4" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
            </ul>
        </div>

        <div>
            <table class="table table-centered table-nowrap rounded">
                <thead class="thead-light">
                    @if(!empty($batchActions))
                        <th class="batch-all-toggler text-center py-1"><span class="rounded-pill placeholder placeholder-lg w-50 bg-secondary"></span></th>
                    @endif

                    @foreach($columnsConfig as $col)
                        <th><div class="rounded-pill placeholder placeholder-lg w-50 bg-secondary"></div></th>
                    @endforeach

                    @if(!empty($actionButtons))
                        <th class="text-end" style="width: {{ count($actionButtons)*60 }}px"><div class="rounded-pill placeholder placeholder-lg bg-secondary" style="width: 20px"></div></th>
                    @endif
                </thead>

                <tbody>
                    @for($i=0; $i < 5; $i++)
                        <tr aria-hidden="true">
                            @if(!empty($batchActions))
                                <td class="batch-item-toggler text-center py-2"><span class="rounded-pill placeholder placeholder-lg w-50 bg-secondary"></span></td>
                            @endif

                            @foreach($columnsConfig as $col)
                                <td><div class="rounded-pill placeholder placeholder-lg w-50 bg-secondary"></div></td>
                            @endforeach

                            @if(!empty($actionButtons))
                                <td class="action-buttons">
                                    @for($j=0; $j<count($actionButtons); $j++)
                                        <span class="rounded bg-secondary placeholder placeholder-lg" style="width: 45px; height: 30px; margin-top: 2px;"></span>
                                    @endfor
                                </td>
                            @endif
                        </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div>

    @script()
        <script>
            window.addEventListener('livewire:navigated', function () {
                let wrapper = document.getElementById('{{ $tableId }}_wrapper');
                Livewire.dispatch('datagrid-screen-size-resolved', {
                    width: ( window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth ),
                    height: ( window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight ),
                    offsetTop: wrapper ? wrapper.offsetTop : 0
                });
            });
        </script>
    @endscript
</div>
