<?php

return [
    'labels' => [
        'search-input' => 'Hľadať',
        'filters-btn' => 'Filtre',
        'table-settings' => 'Nastavenia tabuľky',
        'reset-search' => 'Zrušiť',
        'clear-all-filters' => 'Zrušiť všetky filtre',
        'rows-per-page' => 'Položiek na stranu',
        'rows' => 'položiek',
        'rows-auto-size' => 'Automaticky',
        'save-settings-btn' => 'Uložiť nastavenia',
        'filter' => 'Filter',
        'visible-columns' => 'Viditeľné stĺpce',
        'close' => 'Zatvoriť',
        'previous' => 'Predchádzajúca',
        'next' => 'Nasledujúca',
        'items' => 'Položky',
        'total' => 'celkovo',
        'selected-n-items' => '[1] Vybraná :num položka|[2,4] Vybrané :num položky|Vybraných :num položiek',
    ],

    'messages' => [
        'search-empty-results' => 'Žiadna položka vyhovujúca kritériám vyhľadávania.',
        'filter-empty-results' => 'Žiadna položka vyhovujúca aktívnym filtrom.',
        'no-records' => 'Žiadna dostupná položka.',
        'loading-data' => 'Načítavanie údajov',
    ]
];
