<?php

return [
    'labels' => [
        'search-input' => 'Search',
        'filters-btn' => 'Filters',
        'table-settings' => 'Table settings',
        'reset-search' => 'Reset',
        'clear-all-filters' => 'Clear all filters',
        'rows-per-page' => 'Rows per page',
        'rows' => 'rows',
        'rows-auto-size' => 'Auto size',
        'save-settings-btn' => 'Save settings',
        'filter' => 'Filter',
        'visible-columns' => 'Visible columns',
        'close' => 'Close',
        'previous' => 'Previous',
        'next' => 'Next',
        'items' => 'Items',
        'total' => 'total',
        'selected-n-items' => 'Selected :num item|Selected :num items',
    ],

    'messages' => [
        'search-empty-results' => 'No item matching the search criteria.',
        'filter-empty-results' => 'No item available for this filter combination',
        'no-records' => 'No item available.',
        'loading-data' => 'Loading data',
    ]
];
