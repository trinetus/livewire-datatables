<?php

namespace Trinetus\LivewireDatatables\Enums;

enum HtmlInputType
{
    case TEXT;
    case TEXTAREA;
    case SELECT;
    case DATE;
    case DATETIME;
    case CHECKBOX_GROUP;
}