<?php

namespace Trinetus\LivewireDatatables\Models;

use Illuminate\Database\Eloquent\Model;
use Trinetus\LivewireDatatables\Components\DatagridConfigObject;

/**
 * @property int $id
 * @property string $datagrid_id
 * @property int $user_id
 * @property mixed $config
 */
class DatagridConfig extends Model
{
    protected $table = 'datagrid_config';

    protected $fillable = [
        'datagrid_id',
        'user_id',
        'config'
    ];
    
    protected $casts = [
        'datagrid_id' => 'string',
        'user_id' => 'integer',
        'config' => 'array'
    ];
    
    public static function saveConfig(string $datagridId, ?int $userId, ?int $perPage, ?array $columnsConfig): void
    {
        $columnsNames = collect($columnsConfig)->reject(fn($val) => !$val)->keys()->toArray();
        
        $item = self::where(['datagrid_id' => $datagridId, 'user_id' => $userId])->first();
        $changed = false;
        
        if(empty($item)) {
            $item = new self();
            $item->datagrid_id = $datagridId;
            $item->user_id = $userId;
            
            $changed = true;
        }
        
        if(empty($item->config) && !empty($columnsNames)) {
            $item->config = new DatagridConfigObject($perPage, $columnsNames);
            $changed = true;
        } else {
            $cfg = $item->config;
            
            if(!isset($cfg['perPage']) || $cfg['perPage'] != $perPage) {
                $cfg['perPage'] = $perPage;
                $changed = true;
            }
            
            if(!isset($cfgconfig['columns']) || $cfg['columns'] != $columnsNames) {
                $cfg['columns'] = $columnsNames;
                $changed = true;
            }
            
            $item->config = $cfg;
        }
        
        if($changed) {
            $item->save();
        }
    }
    
    public static function getConfig(string $datagridId, ?int $userId): ?DatagridConfigObject
    {
        $record = self::where(['datagrid_id' => $datagridId, 'user_id' => $userId])->first();
        
        return $record ? 
            new DatagridConfigObject($record->config['perPage'] ?? 10, $record->config['columns'] ?? []) : 
            null;
    }
}
