<?php

namespace Trinetus\LivewireDatatables;

use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Trinetus\LivewireDatatables\Livewire as LivewireComponents;

class LaravelServiceProvider extends ServiceProvider
{

    public function register(){
        parent::register();
        
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'livewire-datatables');

        // TODO: fix precedence
        $this->mergeConfigFrom(
            __DIR__.'/../config/livewire-datatables.php', 'livewire-datatables'
        );
    }
    
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {            
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        }


        $this->loadViewsFrom(__DIR__.'/../resources/views', 'livewire-datatables');

        Livewire::component('livewire-datatables::datagrid', LivewireComponents\Datagrid::class);
    }

}
