<?php

namespace Trinetus\LivewireDatatables\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Livewire\Attributes\On;
use Trinetus\LivewireDatatables\Components\Column;
use Trinetus\LivewireDatatables\Components\FilterTypes\FilterTypeInterface;
use Trinetus\LivewireDatatables\Models\DatagridConfig;

abstract class AbstractListingComponent extends AbstractComponent
{
    public array $activeFilters = [];
    public array $columnsVisibility = [];
    public int $page = 1;
    public int $pageLimit = 10;
    public int $pagesTotal = 1;
    public int $pageScreenSize = 0;
    /** @var int Contains space between top border of .datagrid-wrapper and top border of table's tbody + spacing between end of table and .datagrid-wrapper. !!! Only modify if modified height of datagrid spacings or topbar (search + pagination area) !!! */
    protected int $pageAutoSizeGridHeaderOffset = 110 + 20; // px
    /** @var int Additional offset to fit datagrid to the bottom edge of visible part of page - ex. padding/margin of body/content wrapper */
    protected int $pageAutoSizeGridAdditionalOffset = 20; // px
    /** @var int Size of single row in tbody. This can be overriden per datagrid instance. */
    protected int $pageAutoSizeGridSingleRowHeight = 38; // px
    public array $rows = [];
    public int $rowsTotal = 0;
    public string $searchText = '';
    public string $sortColumn = '';
    public string $sortDirection = 'asc';


    /**
     * @return array<\Trinetus\LivewireDatatables\Components\Column>
     */
    abstract protected function columnsConfig(): array;
    abstract protected function datagridName(): string;
    abstract protected function eloquentModel(): string;
    abstract protected function resourceTransformer(): string;

    protected function modifyDataQuery(Builder &$qb): void
    {
        // overrideable for modify query builder in loadData()
    }

    final public function mount(): void
    {
        $config = DatagridConfig::getConfig($this->datagridName(), auth()->id());

        if(isset($config->perPage)) {
            $this->pageLimit = $config->perPage;
        }

        if(!empty($config->columns)) {
            $this->columnsVisibility = collect($this->columnsConfig())
                        ->mapWithKeys(fn($column) => [$column->getSelectName() => in_array($column->getSelectName(), $config->columns)])
                        ->toArray();
        }

        $sessionSearch = session('datagrid-'.$this->datagridName().'-search');
        if(!empty($sessionSearch)) {
            $this->searchText = $sessionSearch;
        }

        $sessionFilters = session('datagrid-'.$this->datagridName().'-filters');
        if(!empty($sessionFilters)) {
            $this->activeFilters = $sessionFilters;
        }

        $this->loadData();
    }

    protected function filters(): array
    {
        return [];
    }

    protected function actionButtons(): array
    {
        return [];
    }

    protected function batchActions(): array
    {
        return [];
    }

    #[On('datagrid-reload-data')]
    final public function loadData(): void
    {
        $eloquentClassName = $this->eloquentModel();
        $resourceClassName = $this->resourceTransformer();

        $qb = $eloquentClassName::query();

        $this->modifyDataQuery($qb);
        $this->applySearch($qb);
        $this->applyFilters($qb);
        $this->applySorting($qb);

        $this->rowsTotal = (clone $qb)->count();
        $this->pagesTotal = ceil($this->rowsTotal / ($this->pageLimit === 0 ? 1 : $this->pageLimit));

        $rows = $qb->offset(($this->page-1) * $this->pageLimit)
                        ->limit($this->pageLimit === 0 ? 10 : $this->pageLimit)
                        ->get();

        $this->rows = $resourceClassName::collection($rows)
            ->toArray(app('request'));
    }

    public function hasFilters(): bool
    {
        return count($this->filters()) > 0;
    }

    public function hasBatchActions(): bool
    {
        return count($this->batchActions()) > 0;
    }

    #[On('datagrid-search-changed')]
    public function datagridSearchChanged($searchText): void
    {
        $this->searchText = $searchText;
        session(['datagrid-'.$this->datagridName().'-search' => $searchText]);
        $this->loadData();
    }

    #[On('datagrid-filter-changed')]
    public function datagridFilterChanged($activeFilters): void
    {
        $this->activeFilters = $activeFilters;
        session(['datagrid-'.$this->datagridName().'-filters' => $activeFilters]);
        $this->loadData();
    }

    #[On('datagrid-page-changed')]
    public function datagridPageChanged(int $page): void
    {
        $this->page = $page;
        $this->loadData();
    }

    #[On('datagrid-settings-changed')]
    public function datagridSettingsChanged(?int $pageLimit = null, ?array $columnsVisibility = null, bool $configSave = true): void
    {
        if(isset($pageLimit)) {
            $this->pageLimit = in_array($pageLimit, config('livewire-datatables.settings.pagination-options')) ? $pageLimit : 0;
        }

        if(!empty($columnsVisibility)) {
            $this->columnsVisibility = $columnsVisibility;
        }

        if($configSave) {
            DatagridConfig::saveConfig($this->datagridName(), auth()->id(), $this->pageLimit, $this->columnsVisibility);
        }

        if($this->pageLimit === 0 && $this->pageScreenSize > 0) {
            $this->pageLimit = $this->pageScreenSize;
        }

        $this->loadData();
    }

    #[On('datagrid-sort-changed')]
    public function datagridSortChanged(string $columnName, string $direction): void
    {
        $this->sortColumn = $columnName;
        $this->sortDirection = $direction;

        $this->loadData();
    }

    #[On('datagrid-screen-size-resolved')]
    public function windowScreenSizeResolved(int $width, int $height, int $offsetTop): void
    {
        $this->pageScreenSize = floor(
            ($height - $offsetTop - $this->pageAutoSizeGridHeaderOffset - $this->pageAutoSizeGridAdditionalOffset)
                / $this->pageAutoSizeGridSingleRowHeight
        );

        if($this->pageLimit === 0) {
            $this->pageLimit = $this->pageScreenSize;
        }

        $this->loadData();
    }

    #[On('datagrid-batch-action-do')]
    public function batchActionDo(string $actionName, array $selectedRows): void
    {
        $methodName = sprintf("doBatchAction%s", ucfirst($actionName));
        if(method_exists($this, $methodName)) {
            $this->{$methodName}($selectedRows);
        }

        $this->loadData();
    }

    protected function applyFilters(Builder $qb): void
    {
        collect($this->filters())->each(function(FilterTypeInterface $column) use ($qb) {
            $this->{$column->getCallbackFunction()}($qb);
        });
    }

    protected function applySearch(Builder $qb): void
    {
        if(empty($this->searchText)) {
            return;
        }

        $words = explode(' ', $this->searchText);

        $qb->where(function($q) use($words) {
            collect($this->columnsConfig())->each(function(Column $column) use ($q, $words) {
                if($column->isSearchable()) {
                    $q->orWhere(function($q2) use($column, $words) {
                        foreach($words as $word) {
                            $q2->where($column->getFullName(), 'LIKE', '%'.$word.'%');
                        }
                    });
                }
            });
        });
    }

    protected function applySorting(Builder $qb): void
    {
        if(!empty($this->sortColumn)) {
            $qb->orderBy($this->sortColumn, $this->sortDirection === 'desc' ? 'desc' : 'asc');
        }
    }
}
