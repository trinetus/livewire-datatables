<?php

namespace Trinetus\LivewireDatatables\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Attributes\Lazy;
use Livewire\Attributes\Reactive;
use Livewire\Component;
use Trinetus\LivewireDatatables\Components\Column;
use Trinetus\LivewireDatatables\Components\Traits as ComponentTraits;

#[Lazy]
class Datagrid extends Component
{
    use ComponentTraits\DatagridPagination,
        ComponentTraits\DatagridFilters,
        ComponentTraits\DatagridBatchActions;

    protected int $tries = 0;
    public string $tableId;
    public string $searchText = '';
    #[Reactive]
    public array $rows = [];
    #[Reactive]
    public int $rowsTotal = 0;
    public ?int $rowsPerPage = null;
    public array $columnsConfig = [];
    public array $columnsVisibility = [];
    public array $actionButtons = [];
    public string $sortColumn = '';
    public string $sortDirection = 'asc';


    public function rendering(): void
    {
        if(empty($this->columnsVisibility)) {
            foreach($this->columnsConfig as $col) {
                $this->columnsVisibility[$col->getFullName()] = $col->isVisible();
            }
        }

        if($this->rowsPerPage === null) {
            $this->rowsPerPage = $this->pageLimit;
        }
    }

    public function placeholder(): View
    {
        return view(
            config('livewire-datatables.views.placeholder', 'livewire-datatables::livewire.placeholder'),
            [ 'screenSizeCheck' => ($this->pageLimit === 0), 'tableId' => $this->tableId ]
        );
    }

    public function render(): View
    {
        if($this->pageLimit === 0) {
            return $this->placeholder();
        }

        return view(
            config('livewire-datatables.views.datagrid', 'livewire-datatables::livewire.datagrid')
        );
    }

    public function isColumnVisible(Column $column): bool
    {
        if(!empty($this->columnsVisibility)) {
            return !empty($this->columnsVisibility[$column->getSelectName()]);
        }

        return $column->isVisible();
    }

    public function sortBy(string $columnName): void
    {
        if($this->sortColumn === $columnName) {
            $this->sortDirection = ($this->sortDirection === 'asc' ? 'desc' : 'asc');
        }
        $this->sortColumn = $columnName;

        $this->dispatch('datagrid-sort-changed', columnName: $columnName, direction: $this->sortDirection);
    }

    public function search(): void
    {
        $this->dispatch('datagrid-search-changed', searchText: $this->searchText);
    }

    public function resetSearch(): void
    {
        $this->searchText = '';
        $this->dispatch('datagrid-search-changed', searchText: $this->searchText);
    }

    public function saveTableSettings(): void
    {
        $this->dispatch('datagrid-settings-changed', pageLimit: $this->rowsPerPage, columnsVisibility: $this->columnsVisibility);
    }

}
