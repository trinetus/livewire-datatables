<?php

namespace Trinetus\LivewireDatatables\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

abstract class AbstractComponent extends Component
{
    abstract protected function pageTitle(): string;

    abstract protected function viewPath(): string;

    protected function viewAdditionalProperties(): array
    {
        return [];
    }

    public final function render(): View
    {
        $properties = array_merge(
            ['title' => $this->pageTitle()],
            $this->viewAdditionalProperties()
        );

        return view($this->viewPath(), $properties)
            ->layout(config('livewire.layout'), $properties);
    }
}
