<?php

namespace Trinetus\LivewireDatatables\Components;

use Livewire\Wireable;

class ToolbarButton extends ActionButton implements Wireable
{
    protected string $btnSizeClass = "";
    
    protected function formatHref(): string
    {
        if(!empty($this->routeName)) {
            $this->href = route($this->routeName, $this->routeParams);
        }
        
        return $this->href;
    }
}