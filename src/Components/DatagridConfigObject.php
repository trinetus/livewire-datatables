<?php

namespace Trinetus\LivewireDatatables\Components;

class DatagridConfigObject
{
    public function __construct(
        public int $perPage = 10,
        public array $columns = [],
    ) {}
}