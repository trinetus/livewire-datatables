<?php

namespace Trinetus\LivewireDatatables\Components\Traits;

use Livewire\Attributes\Reactive;

trait DatagridPagination
{
    public int $page = 1;
    #[Reactive]  // TODO: fix?
    public int $pageLimit = 10;
    public int $pageScreenSize = 0;
    #[Reactive]
    public int $pagesTotal = 1;


    public function setPage(int $page): void
    {
        $this->page = $page;
        $this->dispatch('datagrid-page-changed', page: $this->page);
    }

    public function isPageAutoSizeEnabled(): bool
    {
        return in_array(0, config('livewire-datatables.settings.pagination-options'));
    }
}
