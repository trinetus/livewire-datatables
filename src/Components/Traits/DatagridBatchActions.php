<?php

namespace Trinetus\LivewireDatatables\Components\Traits;


trait DatagridBatchActions
{
    public array $batchActions = [];
    public array $selectedRows = [];
    
    public function doBatchAction(string $actionName): void
    {   
        $this->dispatch('datagrid-batch-action-do', actionName: $actionName, selectedRows: $this->selectedRows);
        $this->selectedRows = [];
    }
    
    public function cancelBatchActionsSelection(): void
    {
        $this->selectedRows = [];
    }
    
    public function toggleRowSelection(int $rowId): void
    {
        $idx = array_search($rowId, $this->selectedRows);
        
        if ($idx !== false) {
            array_splice($this->selectedRows, $idx, 1, []);
        } else {
            $this->selectedRows[] = $rowId;
        }
    }
    
    public function toggleAllRowsSelection(): void
    {
        if($this->hasAllSelectedRows()) {
            $this->selectedRows = [];
        } else {
            $this->selectedRows = collect($this->rows)->pluck('id')->toArray();
        }
    }
    
    
    public function hasAllSelectedRows(): bool
    {
        return $this->selectedRowsCount() === count($this->rows);
    }
    
    public function selectedRowsCount(): int
    {
        return count($this->selectedRows);
    }
    
    public function hasSelectedRows(): bool
    {
        return $this->selectedRowsCount() > 0;
    }
    
    public function isItemSelected(int $rowId): bool
    {
        return in_array($rowId, $this->selectedRows);
    }
}