<?php

namespace Trinetus\LivewireDatatables\Components\Traits;

trait DatagridFilters
{
    public array $filters = [];
    public array $activeFilters = [];
    
    
    public function hasActiveFilters(): bool
    {
        return $this->activeFiltersCount() > 0;
    }
    
    public function activeFiltersCount(): int
    {
        return collect($this->activeFilters)
            ->reject(function($f) { return empty($f); })
            ->count();
    }
    
    public function filterChanged(): void
    {
        $this->dispatch('datagrid-filter-changed', activeFilters: $this->activeFilters);
    }
    
    public function filterClear(): void
    {
        $this->activeFilters = [];
        $this->dispatch('datagrid-filter-changed', activeFilters: $this->activeFilters);
    }
    
}