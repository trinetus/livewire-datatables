<?php

namespace Trinetus\LivewireDatatables\Components\FilterTypes;

use Livewire\Wireable;

class AbstractType implements Wireable
{
    public function __construct(
        protected readonly string $name = '',
        protected readonly string $label = '',
        protected readonly string $callbackFunction = '',
        /** @var array<int|string, string> */
        protected array $attributes = [],
        /** @var array<int|string, string> */
        protected readonly array $options = [],
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getCallbackFunction(): string
    {
        return $this->callbackFunction;
    }

    public function getAlpineInputChangeEventName(): string
    {
        return "@change";
    }

    protected function formatAttributes(): string
    {
        $out = [];
        foreach ($this->attributes as $key=>$value) {
            $out[] = sprintf('%s="%s"', $key, $value);
        }

        // fix offcanvas closing behavior (double-click reopen bug) with alpine @click callback
        $out[] = sprintf('%s="(new bootstrap.Offcanvas(\'#offcanvasDatagridFilter\')).hide()"', $this->getAlpineInputChangeEventName());

        return implode(' ', $out);
    }

    public function toLivewire()
    {
        return [
            "name" => $this->name,
            "label" => $this->label,
            "callbackFunction" => $this->callbackFunction,
            "attributes" => $this->attributes,
            "options" => $this->options
        ];
    }

    public static function fromLivewire($value)
    {
        return new static(
            $value["name"], 
            $value["label"], 
            $value["callbackFunction"], 
            $value["attributes"], 
            $value["options"]
        );
    }
}
