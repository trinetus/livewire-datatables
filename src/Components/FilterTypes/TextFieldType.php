<?php

namespace Trinetus\LivewireDatatables\Components\FilterTypes;

class TextFieldType extends AbstractType implements FilterTypeInterface
{
    public function htmlOutput(): string
    {
        return sprintf('<input id="dg_filter_%s" wire:model="activeFilters.%s" wire:change="filterChanged" name="%s" %s>',
            $this->name,
            $this->name,
            $this->name,
            $this->formatAttributes(),
        );
    }
}
