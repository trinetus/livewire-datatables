<?php

namespace Trinetus\LivewireDatatables\Components\FilterTypes;

class OptionsListType extends AbstractType implements FilterTypeInterface
{
    public function htmlOutput(): string
    {
        return sprintf('<select id="dg_filter_%s" wire:model="activeFilters.%s" wire:change="filterChanged" name="%s" %s>%s</select>', 
            $this->name, 
            $this->name, 
            $this->name, 
            $this->formatAttributes(), 
            $this->formatOptions()
        );
    }

    protected function formatOptions(): string
    {
        $out = [];
        foreach ($this->options as $key=>$label) {
            $out[] = sprintf('<option value="%s">%s</option>', $key, $label);
        }
        return implode(' ', $out);
    }
}
