<?php

namespace Trinetus\LivewireDatatables\Components\FilterTypes;

interface FilterTypeInterface
{
    public function htmlOutput(): string;
}
