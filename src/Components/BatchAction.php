<?php

namespace Trinetus\LivewireDatatables\Components;

use Livewire\Wireable;
use Trinetus\LivewireDatatables\Enums\VariantType;

class BatchAction implements Wireable
{
    protected string $btnSizeClass = "btn-sm";

    public function __construct(
        public string $name = '',
        public string $variant = VariantType::INFO->value,
        public string $label = '',
        public string $iconClass = '',
        public array $options = [],
    ) {}

    public function htmlOutput(): string
    {
        return sprintf('<a href="%s" class="btn %s %s" %s>%s%s</a>',
            $this->formatHref(),
            $this->btnSizeClass,
            $this->variant,
            $this->formatOptions(),
            $this->formatIcon(),
            $this->label
        );
    }

    protected function formatIcon(): string
    {
        if(empty($this->iconClass)) {
            return '';
        }

        return sprintf('<i class="%s me-2"></i>', $this->iconClass);
    }

    protected function formatOptions(): string
    {
        $strArr = [];
        foreach($this->options as $key => $value) {
            $strArr .= sprintf('%s="%s"', $key, $value);
        }

        return join(' ', $strArr);
    }

    public function toLivewire()
    {
        return [
            "name" => $this->name,
            "variant" => $this->variant,
            "label" => $this->label,
            "iconClass" => $this->iconClass,
            "options" => json_encode($this->options),
        ];
    }

    public static function fromLivewire($value)
    {
        return new static(
            $value["name"],
            $value["variant"],
            $value["label"],
            $value["iconClass"],
            json_decode($value["options"], associative: true)
        );
    }
}
