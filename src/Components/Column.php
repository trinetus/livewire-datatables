<?php

namespace Trinetus\LivewireDatatables\Components;

use Livewire\Wireable;
use Trinetus\LivewireDatatables\Components\ColumnFormatters\ColumnFormatterInterface;
use Trinetus\LivewireDatatables\Components\ColumnFormatters\StringFormatter;

class Column implements Wireable
{
    protected ColumnFormatterInterface $columnFormatter;

    public function __construct(
        public readonly string $name,
        public readonly string $label,
        public readonly string $formatterClass = StringFormatter::class,
        public readonly bool $isSortable = false,
        public readonly bool $isSearchable = false,
        public readonly bool $isVisible = true,
        public readonly ?string $aliasName = null,
        public readonly string $tdClass = '',
        public readonly string $thClass = ''
    ) {
        $fc = $this->formatterClass;
        $this->columnFormatter = new $fc();
    }

    public function getName(): string
    {
        if(strstr($this->name, '.') === false) {
            return $this->name;
        }

        $parts = explode('.', $this->name);

        return $parts[1] ?? $parts[0];
    }

    public function getFullName(): string
    {
        return $this->name;
    }

    public function getSelectName(): string
    {
        return $this->aliasName ?? $this->getName();
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function isSearchable(): bool
    {
        return $this->isSearchable;
    }

    public function isSortable(): bool
    {
        return $this->isSortable;
    }

    public function isVisible(): bool
    {
        return $this->isVisible;
    }

    public function isEnabledUnescaped(): bool
    {
        return $this->columnFormatter->enableUnescaped();
    }

    public function formatValue(mixed $value): string
    {
        return $this->columnFormatter->format($value);
    }

    public function toLivewire()
    {
        return [
            "name" => $this->getName(),
            "label" => $this->getLabel(),
            "formatterClass" => $this->formatterClass,
            "isSortable" => $this->isSortable(),
            "isSearchable" => $this->isSearchable(),
            "isVisible" => $this->isVisible(),
            "selectName" => $this->getSelectName(),
            "tdClass" => $this->tdClass,
            "thClass" => $this->thClass,
            "isEnabledUnescaped" => $this->isEnabledUnescaped(),
        ];
    }

    public static function fromLivewire($value)
    {
        return new static(
            $value["name"],
            $value["label"],
            $value["formatterClass"],
            $value["isSortable"],
            $value["isSearchable"],
            $value["isVisible"],
            $value["selectName"],
            $value["tdClass"],
            $value["thClass"],
        );
    }
}
