<?php

namespace Trinetus\LivewireDatatables\Components;

use Livewire\Wireable;
use Trinetus\LivewireDatatables\Enums\VariantType;

class ActionButton implements Wireable
{
    private mixed $rowItem;
    protected string $btnSizeClass = "btn-sm";

    public function __construct(
        public string $variant = VariantType::INFO->value,
        public string $label = '',
        public string $iconClass = '',
        public string $href = '',
        public string $routeName = '',
        public array $routeParams = [],
        public string $customLinkClass = ''
    ) {}

    public function setRowItem(mixed $rowItem): self
    {
        $this->rowItem = $rowItem;

        return $this;
    }

    public function htmlOutput(mixed $rowItem): string
    {
        $html = sprintf('<a href="%s" class="btn %s btn-%s %s">%s%s</a>', 
            $this->formatHref(), 
            $this->btnSizeClass, 
            $this->variant, 
            $this->customLinkClass, 
            $this->formatIcon(), 
            $this->label
        );

        if(!empty($rowItem)) {
            $html = str_replace(
                array_map(static function($it) { return '{'.$it.'}'; }, array_keys($rowItem)),
                array_values($rowItem),
                $html
            );
        }

        return $html;
    }

    protected function formatIcon(): string
    {
        if(empty($this->iconClass)) {
            return '';
        }

        return sprintf('<i class="%s me-2"></i>', $this->iconClass);
    }

    protected function formatHref(): string
    {
        if(empty($this->routeName)) {
            return $this->href;
        }

        $params = [];
        if(!empty($this->routeParams)) {
            foreach($this->routeParams as $param) {
                $params[$param] = $this->rowItem[$param];
            }
        }

        return route($this->routeName, array_values($params));
    }

    public function toLivewire()
    {
        return [
            "variant" => $this->variant,
            "label" => $this->label,
            "iconClass" => $this->iconClass,
            "href" => $this->href,
            "routeName" => $this->routeName,
            "routeParams" => $this->routeParams,
            "customLinkClass" => $this->customLinkClass
        ];
    }

    public static function fromLivewire($value)
    {
        return new static(
            $value["variant"], 
            $value["label"], 
            $value["iconClass"], 
            $value["href"], 
            $value["routeName"], 
            $value["routeParams"], 
            $value["customLinkClass"]
        );
    }
}
