<?php

namespace Trinetus\LivewireDatatables\Components\ColumnFormatters;

use Carbon\Carbon;

class DateTimeFormatter implements ColumnFormatterInterface
{
    public function format(mixed $raw): string
    {
        if($raw === null) {
            return '';
        }

        return Carbon::parse($raw)->format(config('livewire-datatables.settings.datetime-format'));
    }

    public function enableUnescaped(): bool
    {
        return false;
    }
}
