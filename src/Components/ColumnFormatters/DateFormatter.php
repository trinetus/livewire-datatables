<?php

namespace Trinetus\LivewireDatatables\Components\ColumnFormatters;

use Carbon\Carbon;

class DateFormatter implements ColumnFormatterInterface
{
    public function format(mixed $raw): string
    {
        if($raw === null) {
            return '';
        }

        $dt = ($raw instanceof Carbon) ? $raw : Carbon::parse($raw);
        return (!empty($dt) ? $dt->format(config('livewire-datatables.settings.date-format')) : $raw);
    }

    public function enableUnescaped(): bool
    {
        return false;
    }
}
