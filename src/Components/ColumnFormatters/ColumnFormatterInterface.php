<?php

namespace Trinetus\LivewireDatatables\Components\ColumnFormatters;

interface ColumnFormatterInterface
{
    public function format(mixed $value): string;

    public function enableUnescaped(): bool;
}
