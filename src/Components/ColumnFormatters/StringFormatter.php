<?php

namespace Trinetus\LivewireDatatables\Components\ColumnFormatters;

class StringFormatter implements ColumnFormatterInterface
{

    public function format(mixed $raw): string
    {
        return (string) $raw;
    }

    public function enableUnescaped(): bool
    {
        return false;
    }
}
