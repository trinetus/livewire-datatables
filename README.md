# Introduction

Livewire datatables is simple set of components for making nice and good UX datagrids and datatables easily and fast.

### <span style="color: red">WARNING: This is still experimental package, do not use in production (or at your own risk). Please wait for version 1.0</span>

## Preconditions

- Livewire 3+
- Laravel 10+
- PHP 8.2+
- Bootstrap 5


## Installation

### Install from packagist

```
composer install trinetus/livewire-datatables
```

### Add service provider

1. open config file: `config/app.php`,
2. search for `providers` array key,
3. move to the end of **Package providers** group and add new line:

```
Trinetus\LivewireDatatables\LivewireDatatablesServiceProvider::class,
```


## Configuration and customizations

### Livewire configuration 
for more descriptive informations see official Livewire doc:
https://livewire.laravel.com/docs/installation

1. Publish livewire (if not already done) and datatables configs:
    ````
        php artisan livewire:publish --config
        php artisan livewire-datatables:publish --config
    ````
2. Setup livewire base layout and modify in `config/livewire.php`:
    ````
        'layout' => 'path.to.livewire.layout.blade'
    ````
3. Don't forget to define livewire's `@livewireStyles` and `@livewireScriptConfig` in your layout blade.
4. Configure desired icon set in `config/livewire-datatables.php` config:
    ````
        'icon-set' => 'bootstrap-icons'
    ````

### Include admin template scss file and link built css to template
- Include scss file into your scss parent file (please double check proper relative path)

````
@import "../../vendor/trinetus/livewire-datatables/resources/scss/datatables.scss";
````

## Create first datagrid page

**WIP: description, examples**

1. create component file (ext from vendor AbstractDatagridComponent) and customize it to your needs:
````
<?php

namespace App\Livewire;

use Trinetus\LivewireDatatables\Components\Column;
use Trinetus\LivewireDatatables\Components\ColumnFormatters\DateFormatter;
use Trinetus\LivewireDatatables\Components\ColumnFormatters\StringFormatter;
use Trinetus\LivewireDatatables\Livewire\AbstractListingComponent;
use App\Resources\ExampleResource;

class ExampleDatagrid extends AbstractListingComponent
{
    public string $sortColumn = 'name'; // default

    protected function datagridName(): string
    {
        return "example-listing";
    }

    protected function pageTitle(): string
    {
        return "All items";
    }

    protected function viewPath(): string
    {
        return "livewire.example";
    }

    protected function eloquentModel() : string
    {
        return MyModel::class;
    }

    protected function resourceTransformer() : string
    {
        return ExampleResource::class;
    }

    public function columnsConfig(): array
    {
        return [
            new Column('id', 'ID', StringFormatter::class, false, true, false),
            new Column('name', 'Name', StringFormatter::class, true, true),
            new Column('created_at', 'Created at', DateFormatter::class, true, true),
        ];
    }

    public function actionButtons(): array
    {
        return [
            //new ActionButton(VariantType::OUTLINE_INFO->value, 'Edit', 'bi-pencil', '', 'route.path.to.edit.view', ['id']),
            //new ActionButton(VariantType::OUTLINE_DANGER->value, 'Delete', 'bi-trash', '#'),
        ];
    }

    public function batchActions() : array
    {
        return [
        ];
    }

    public function filters(): array
    {
        return [
        ];
    }
}

````

2. create view (call vendor component)
````
<div>
    <h1>Example datagrid</h1>

    <livewire:livewire-datatables::datagrid
        tableId="example-listing"
        :actionButtons="$this->actionButtons()"
        :batchActions="$this->batchActions()"
        :columnsConfig="$this->columnsConfig()"
        :filters="$this->filters()"
        :$columnsVisibility
        :$activeFilters
        :$page
        :$pageLimit
        :$pagesTotal
        :$pageScreenSize
        :$rows
        :$rowsTotal
        :$searchText
        :$sortColumn
        :$sortDirection
    />
</div>
````

3. Assign component to route (as full page component):
````
    Route::get('example', \App\Livewire\ExampleDatagrid::class)->name('page-example');
````

## Advanced configuration

**WIP**

## License
MIT License - see LICENSE file.
