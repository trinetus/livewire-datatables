<?php

return [
    'settings' => [
        'datetime-format' => 'd.m.Y H:i:s',
        'date-format' => 'd.m.Y',
        'pagination-options' => [ 0, 10, 15, 25, 50, 100 ]
    ],

    'ui' => [
        /**
         * You can specify custom icon classes. Default classes uses Bootstrap icons icon set.
         */
        'icons' => [
            'search-cancel' => 'bi bi-x-lg',
            'table-settings' => 'bi bi-gear-fill',
            'settings-save-btn' => 'bi bi-check-lg',
            'filter-btn' => 'bi bi-filter',
            'clear-all-filters-btn' => 'bi bi-x-octagon',
            'search-empty-label' => 'bi bi-search bi--3x',
            'reset-filters-btn' => 'bi bi-x-octagon',
            'reset-search-btn' => 'bi bi-x-octagon',
            'checked-square' => 'bi bi-check-square',
            'unchecked-square' => 'bi bi-square',
            'sort-desc' => 'bi bi-caret-down-fill',
            'sort-asc' => 'bi bi-caret-up-fill',
            'sort-inactive' => 'bi bi-arrow-down-up',
            'no-records-label' => 'bi bi-database-x bi--3x',
            'filter-empty-results' => 'bi bi-filter-circle bi--3x',
            'loading-data' => 'bi bi-arrow-repeat bi--3x',
        ],

        /**
         * You can specify custom classes for buttons. Default classes uses Bootstrap standard or outlined btn-* classes
         */
        'classes' => [
            'btn-filter' => 'btn btn-info ms-2',
            'btn-clear-filters' => 'btn btn-warning',
            'btn-settings-save' => 'btn btn-warning',
        ],
    ],

    /**
     * You can override default views. But use at own risk!
     * Caution: It may be backwards incompatible between major/minor versions!
     */
    'views' => [
        //'datagrid' => 'custom.datagrid.blade',
        //'placeholder' => 'custom.placeholder.blade',
    ],

];
